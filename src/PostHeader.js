import React from 'react';
import PropTypes from 'prop-types';

const PostHeader = props => (
  <header>
    <div className="avatar">
      <img src={props.avatar} alt={props.name} />
    </div>
    <div className="name">{props.name}</div>
    <div className="timestamp">há {props.timestamp} min</div>
  </header>
);

PostHeader.propTypes = {
  avatar: PropTypes.string,
  name: PropTypes.string.isRequired,
  timestamp: PropTypes.number.isRequired,
};

PostHeader.defaultProps = {
  avatar: 'avatar.jpg',
};

export default PostHeader;
