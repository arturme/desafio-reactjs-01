import React from 'react';
import PropTypes from 'prop-types';

const Header = props => <header id="main-header">{props.children}</header>;

Header.defaultProps = {
  children: 'RocketBook',
};

Header.propTypes = {
  children: PropTypes.string,
};

export default Header;
