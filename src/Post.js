import React from 'react';
import PropTypes from 'prop-types';
import PostHeader from './PostHeader';

const Post = props => (
  <div className="post">
    <PostHeader name={props.name} avatar={props.avatar} timestamp={props.timestamp} />
    <p className="description">{props.description}</p>
  </div>
);

Post.propTypes = {
  description: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  name: PropTypes.string.isRequired,
  timestamp: PropTypes.number.isRequired,
};

Post.defaultProps = {
  avatar: 'avatar.jpg',
};

export default Post;
