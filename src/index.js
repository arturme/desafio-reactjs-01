import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import Header from './Header';
import Post from './Post';
import './style.scss';

class App extends Component {
  initialPosts = [
    {
      name: 'Artur Haddad',
      avatar: 'avatar3.png',
      timestamp: 5,
      description:
        'Lorem ipsum eros morbi leo quisque mauris primis, morbi pharetra vestibulum nisi hac potenti, lobortis nec auctor aenean erat ut. luctus euismod velit varius hac risus habitant eu dictumst id, sit ut proin potenti feugiat hendrerit nec tellus lectus, pellentesque fames taciti dictumst ullamcorper donec dapibus aliquet.',
    },
    {
      name: 'John Doe',
      avatar: 'avatar2.jpg',
      timestamp: 10,
      description:
        'Segundo dorem ipsum eros morbi leo quisque mauris primis, morbi pharetra vestibulum nisi hac potenti, lobortis nec auctor aenean erat ut. luctus euismod velit varius hac risus habitant eu dictumst.',
    },
  ];

  state = { posts: this.initialPosts };

  render() {
    return (
      <Fragment>
        <Header />
        <div className="posts">
          {this.state.posts.map((post, i) => (
            <Post
              name={post.name}
              avatar={post.avatar}
              timestamp={post.timestamp}
              description={post.description}
              key={i}
            />
          ))}
        </div>
      </Fragment>
    );
  }
}

render(<App />, document.getElementById('app'));
